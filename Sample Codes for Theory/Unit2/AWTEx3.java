import java.awt.*;  
import java.awt.event.*;
 


//By Creating object of Frame Class

class AWTEx3 implements ActionListener{  

TextField tf;   
Button b1;
Button b2;

AWTEx3(){  

Frame f= new Frame();
  
//create components  
tf=new TextField();  
tf.setBounds(200,100,100,50);  

 b1=new Button("click me");  
b1.setBounds(100,100,100,50);  

b2=new Button("click2 ");  
b2.setBounds(100,200,100,50);  
  
//register listener  
b1.addActionListener(this);//passing current instance 
b2.addActionListener(this);//passing current instance   
  
//add components and set size, layout and visibility  
f.setSize(300,300);  
f.setLayout(null);
f.add(b1);
f.add(b2);
f.add(tf);  
f.setVisible(true);  
 
}


  
public void actionPerformed(ActionEvent ae){  
if(ae.getSource()==b1)
	tf.setText("Welcome");  

if(ae.getSource()==b2)
	tf.setText("AJP");  
} 
 
public static void main(String args[]){  
AWTEx3 a=new AWTEx3();  

}  

}  