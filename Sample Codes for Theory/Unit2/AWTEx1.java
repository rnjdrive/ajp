import java.awt.*;  
import java.awt.event.*;
 


class AWTEx1 extends Frame implements ActionListener{  

TextField tf;   

AWTEx1(){  
  
//create components  
tf=new TextField();  
tf.setBounds(200,100,100,50);  

Button b1=new Button("click me");  
b1.setBounds(100,100,100,50);  
  
//register listener  
b1.addActionListener(this);//passing current instance  
  
//add components and set size, layout and visibility  
setSize(300,300);  
setLayout(null);
add(b1);
add(tf);  
  
setVisible(true);  
}


  
public void actionPerformed(ActionEvent ae){  
tf.setText("Welcome");  
} 
 
public static void main(String args[]){  
AWTEx1 a=new AWTEx1();  

}  

}  