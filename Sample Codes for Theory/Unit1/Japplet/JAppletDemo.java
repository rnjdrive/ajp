import java.awt.*;
import javax.swing.*;
public class JAppletDemo extends JApplet
{
 	JTextField jtf;
 	public void init()
	 {
		Container contentPane = getContentPane();
		contentPane.setLayout(new FlowLayout());
 		
 		ImageIcon ii = new ImageIcon("IC.jpg");
 		JLabel jl = new JLabel("IC", ii, JLabel.CENTER);
 		contentPane.add(jl);


		jtf = new JTextField(15);
 		contentPane.add(jtf);


		JButton jb = new JButton("Click");	
		contentPane.add(jb);

		JButton jc = new JButton("Click1");	
		contentPane.add(jc);


	        JCheckBox cb1 = new JCheckBox("C", true);
                contentPane.add(cb1);
                
                JCheckBox cb2 = new JCheckBox("C++", false);
		contentPane.add(cb2);

		JRadioButton b1 = new JRadioButton("A");
		contentPane.add(b1);

		JRadioButton b2 = new JRadioButton("B",true);
		contentPane.add(b2);


		JComboBox jcb = new JComboBox();
		jcb.addItem("Green");
		jcb.addItem("Red");
		jcb.addItem("Black");
           contentPane.add(jcb);


 	}
}